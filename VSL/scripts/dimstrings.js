/*
Note this script presumes the stock Dim Strings *_L2_Art-Combi matrices
Works in combination with the corresponding articulation set
*/

// x-axis CC range non-uniform, splits 10/63/115
var CCs = [0,11,64,116];

//this maps articIDs to (0-based) cell coords
var idToXY =
[
null, //no id 0
//sus
[1,0],[1,1],[1,2],[1,3]
//shorts
,[0,0],[0,1],[0,3]
//leg
,[3,0],[3,1],[3,2],[3,3]
//perf trills
,[5,0],[5,2]
//portamento
,[4,0],[4,1],[4,2],[4,3]
//rep
,[6,0],[6,1],[6,2],[6,3]
//pizz, col
,[11,0],[11,1],[11,3]
//fp,sfz
,[2,0],[2,1],[2,3]
//cresc/dim, note: repeated, will add A/B
// 28/29, 30/31
,[10,0],[10,1],[10,0],[10,1]
//pfp
,[10,2],[10,3]
//trem
,[8,0],[8,1],[8,3]
//harm
,[9,0],[9,1],[9,2],[9,3]
//reps
,[7,0],[7,1],[7,2],[7,3]
];

var isBass = false;

var PluginParameters = [{name:"Instrument", type:"menu", valueStrings:["Violin", "Viola", "Cello", "Bass"]}];

function ParameterChanged(param, value) {
	if (param == 0) {
		isBass = (value == 3);
  }
}

function HandleMIDI(event)
{
  if(event instanceof NoteOn)
	  {
	  var offms = 5;
		var ksBase = MIDI.noteNumber(isBass ? "C5":"C0");
		var aSwitch = MIDI.noteNumber("A-1");
		var bSwitch = MIDI.noteNumber(isBass ? "A#-1":"B-1");
		var id = event.articulationID;
		var xy = idToXY[id];
	
		var cc = new ControlChange;
		cc.number = 1;
		cc.value = CCs[xy[1]];
		
		var ks = new NoteOn;
		ks.pitch = ksBase + xy[0];
		var ksoff = new NoteOff(ks);
		
		//add AB if cresc/dim
		if(id > 27 && id < 32)
			{
			var ab = new NoteOn;
			ab.pitch = id < 30 ? aSwitch : bSwitch;
			var aboff = new NoteOff(ab);
			ab.send();
			aboff.sendAfterMilliseconds(offms);
			}
		
		cc.send();
		ks.send(); 
		ksoff.sendAfterMilliseconds(offms);
		}
 
	event.trace();
	event.send();
}
