/*
CC Driven dynamics and expression, with velocity accents, for VSL
Sustain pedal toggles vel-xfade

presumes VSL defaults of CC28 for xfade toggle, CC11 expression, CC2 vel-xfade

Rich Hickey 07/2018
*/

var dynCC;
var velSplit;
var accentRange;
var dimRange;
var exprBase;

var PluginParameters = [
{name:"Dynamics input CC", defaultValue:2, minValue:1, maxValue:127, numberOfSteps:126, type:"lin"}
,{name:"Velocity split", defaultValue:40, minValue:10, maxValue:120, numberOfSteps:110, type:"lin"}
,{name:"Accent range", defaultValue:20, minValue:0, maxValue:40, numberOfSteps:40, type:"lin"}
,{name:"Dim range", defaultValue:5, minValue:0, maxValue:40, numberOfSteps:40, type:"lin"}
,{name:"Expression baseline", defaultValue:80, minValue:10, maxValue:120, numberOfSteps:110, type:"lin"}
];

function ParameterChanged(param, value) {
switch(param) {
	case 0:
		dynCC = value;
		break;
	case 1:
		velSplit = value;
		break;
	case 2:
		accentRange = value;
		break;
	case 3:
		dimRange = value;
		break;
	case 4:
		exprBase = value;
		break;
  }
}

//most recent value of dyn CC
var dyn = 0;
//val of dyn when most recent note on
var notedyn = 0;
var xfade = false;

var xfadeToggleCC = 28;
var xfadeCC = 2;
var exprCC = 11;
var susCC = 64;

function HandleMIDI(event)
{
	//event.trace();
	
	if(event instanceof ControlChange)
		{
		if(event.number == susCC)
			{
			xfade = event.value >= 64;
			if(!xfade)
				notedyn = dyn;
			event.number = xfadeToggleCC;
			}
		else if(event.number == dynCC)
			{
			dyn = event.value;
			event.number = xfadeCC;
			if(!xfade)
				{
				var delta = event.value - notedyn;
				var ecc = new ControlChange(event);
				ecc.number = exprCC;
				ecc.value = MIDI.normalizeData(exprBase + delta);
				ecc.send();
				}
			}
		}
	else if(event instanceof NoteOn && event.velocity > 0)
		{
		notedyn = dyn;
		var basecc = new ControlChange(event);
		basecc.number = exprCC;
		basecc.value = exprBase;
		basecc.send();
		var accent = 0;
		var vdelta = event.velocity - velSplit;
		if(!xfade)
			{
			if(accentRange > 0 && vdelta > 0)
				accent = accentRange * vdelta/(127-velSplit);
			else if(dimRange > 0 && vdelta < 0)
				accent = dimRange * vdelta/(velSplit-1);
			event.velocity = Math.max(1,MIDI.normalizeData(dyn + accent));
			}
		}
		
	event.send();
		
}
