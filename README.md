## Logic Scripts

Here are some free scripts and articulation maps for Logic Pro X 10.4+. To get them, [download the repository](../../downloads/). 

### Use
To use the **scripts**, open the `.js` script file in a text editor, then insert the Scripter MIDI plugin on your track. Choose 'Open script in editor' then copy/paste in the script. Make sure you 'Save as' on the scripter plugin so you can reuse the script on other channels and projects.

To use the **articulation sets**, copy the `.plist` file into `'/Users/yourname/Music/Audio Music Apps/Articulation Settings'`

### Scripts
First up we have a [dynamics script](./VSL/scripts/dynamics.js) that gives you dynamic crossfade-like control for VSL with vel. xfade OFF. Thus no phasing etc. See the [rationale and description](./xfade.md) for details.

### Articulation Sets
Next there is an articulation set for VSL Dimension Strings (in `'VSL/artics/Dimension Strings.plist'`). Because Logic 10.4 cannot currently control the VSL matrices (which need more than one message to set the X/Y axis), there is a corresponding Scripter [script](./VSL/scripts/dimstrings.js) which interprets the articulation IDs. If you use this with the dynamics script, place it after the dynamics script. Note that the articulation set has no Switches or Outputs. You can set up whatever Switches you want, but **you must leave the Outputs blank in order for the script to handle the articulations!**

Have fun!

Rich
