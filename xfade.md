# XFade Considered Harmful - Free script for Logic and VSL

Have you ever noticed that while most sample libs feature dynamic crossfade (henceforth 'xfade') for expression control, many of the best demos rarely use it? xfade is an inherent compromise between limited sample levels and the continuous, within-note, dynamics afforded by wind and string instruments. Even the best xfade implementations yield doubling effects on solo instruments, weird phase problems, unnaturally accentuated vibrato, smeared attacks etc. Most sample libraries (where it can be turned off) sound better with it off, but the performance options are then curtailed.

The primary alternative is to leave xfade off, use velocity for the primary dynamic, and add within-note expression with either expression (CC11) or volume (CC7) CC riding. This is challenging to do. First, there must be coordination between the levels implied by the velocity and the CC. Second, unless the expression controller is 'returned-to-baseline' you can easily drift out to one end or the other. Manually returning it yields the all-too-common rollercoaster ride dynamics where each note swells and returns, and a smooth crescendo across notes is quite difficult to do live. Thus I think the demos that do this best have involved a lot of precise manual editing.

I have been experimenting with non-xfade smooth dynamic control for VSL and Chris Hein libs using Bome MIDI translator, and have just ported the VSL implementation to a Logic Scripter script which I am now sharing. 

##Here's how it works:

###A) The instrument is run with xfade off.

###B) The primary dynamic is governed by a CC controller (a parameter to the script). 
This should be a controller that retains its position like a mod wheel, expression pedal, or breath controller (i.e. not pitch bend or aftertouch). The value of this CC is used to determine the *velocity* that the instrument sees (plus/minus accents, see E below).

###C) This *same controller* is tracked (relatively) subsequent to note onset to generate CC11 expression messages. 
Each note-on automatically (and instantly) resets CC11 expression to a baseline value (another param), and subsequent moves shape it from there. This ends up feeling like xfade control - smooth cresc/dims, only swell/return if that's what you want. Of course, this is only modifying the volume, not the timbre. For large dramatic swells you still might want xfade (to get both), so:

###D) The sustain pedal can be used to toggle xfade on a per note/phrase basis. 
This same controller generates xfade, and it picks up and drops off the dynamics right where you were. The sustain message gets eaten (you weren't using the sus pedal for winds/strings, were you?)

###E) Finally, while a wheel/pedal is good for the overall arc, wiggling it around is pretty awkward for accents. 
Many xfade modes throw away the original velocity (or use it for portamento control, please stop doing that :). This script uses the original played velocity as an *accenting delta* on the CC dynamic. So even if you leave the wheel/pedal in the same position, playing harder or more softly increases/decreases the generated velocity. The split point, and pos/neg accent ranges are all params.

##That's it
A single CC gives you p-f and crescendo/diminuendo control, expressive swells etc, and velocity gives you accent variation. Opt in/out of xfade with the sustain pedal. I find this very manageable and expressive, and it lets the pristine sound of the samples shine.

**There's more!** Because the scripter works live while you are playing and again when you are playing back, what's recorded are your original velocities and CC moves. Thus you can tweak your accent ranges and expression baseline after the fact. And you can edit your p-f dynamics independent of your accents and vice-versa. I've found thinking about accents in this relative way to be quite empowering, more musical and less finicky when editing.

**N.B** - This kind of approach can only work with *very consistent* libraries, VSL and CH being good examples.

I think this kind of control should be built into sample libs. Until then, here's the script. Feedback welcome.
